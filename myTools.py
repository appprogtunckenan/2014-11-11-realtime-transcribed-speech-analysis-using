import zmq
import random
import sys
import time
import re
import string
import numpy as np


def sentence_finder(fl):
	sentences = []
	i_temp = 0
	for i in range(0, len(fl)):
 		if fl[i] == '?' or fl[i] == '!':
 			# print fl[i_temp:i+1]
 			sentences.append(fl[i_temp:i+1])
 			# socket.send("%s" % fl[i_temp:i+1])
 			# time.sleep(1)
 			if [i+2] == ' ':
 				i_temp = i+3
 			else:
 				i_temp = i+2
 		if fl[i] == '.':
 			if fl[i-2:i+1] == 'Mr.' or fl[i-3:i+1] == 'Mrs.':
 				continue
 			elif i == len(fl):
 				# print fl[i_temp:i+1]
 				sentences.append(fl[i_temp:i+1])
 				# socket.send("%s" % fl[i_temp:i+1])
 				# time.sleep(1)
 				# close file
 			else:
 				# print fl[i_temp:i+1]
 				sentences.append(fl[i_temp:i+1])
 				# socket.send("%s" % fl[i_temp:i+1])
 				# time.sleep(1)
 				i_temp = i+2
 	return sentences

def ds_pronouns():
    pronoun_list_single =["all","another","any","anybody","anyone","anything","both","each","either","everybody","everyone"
                   "everything","few","he","her","hers","herself","him","himself","his","I","it","its","itself",
    "many"      ,
    "me"        ,
    "mine"      ,
    "more"      ,
    "most"      ,
    "much"      ,
    "myself"    ,
    "neither"   ,
    "no one"    ,
    "nobody"    ,
    "none"      ,
    "nothing"   ,
    "one"       ,
    "other"     ,
    "others"    ,
    "ours"      ,
    "ourselves" ,
    "several"   ,
    "she"       ,
    "some"      ,
    "somebody"  ,
    "someone"   ,
    "something" ,
    "that"      ,
    "their"     ,
    "theirs"    ,
    "them"      ,
    "themselves",
    "these"     ,
    "they"      ,
    "this"      ,
    "those"     ,
    "us"        ,
    "we"        ,
    "what"      ,
    "whatever"  ,
    "which"     ,
    "whichever" ,
    "who"       ,
    "whoever"   ,
    "whom"      ,
    "whomever"  ,
    "whose"     ,
    "you"       ,
    "your"      ,
    "yours"     ,
    "yourself"  ,
    "yourselves"]

    pronoun_collective = ["everybody","everyone","everything","many","their"     ,"theirs"    ,"them"      ,"themselves","these"     ,"they"      ,"those"     ,"us"        ,"we"        ,"yall"]
    pronoun_individual = [    "yours"     ,"yourself"  ,"yourselves","this"      ,"what"      ,"which"    
                            ,"who","another","any","he","her","hers","herself","him","himself","his","I","it","its","itself",]

    pronoun_list_double  = ["one another","each other"]

    green_keyword_list_single = ["c++", "shrubbery", ]
    purple_keyword_list_single =[ "python", "hedge"]
    green_keyword_list = ["you guys", "star trek"]
    purple_keyword_list = ["yall", "star wars"]

    return pronoun_list_double,pronoun_list_single, green_keyword_list, purple_keyword_list,pronoun_collective,pronoun_individual

def single_word_check (word,group_to_compare,updated_value):

    for i in range(len(group_to_compare)):
        if word == group_to_compare[i] or word == group_to_compare[i].capitalize():
            updated_value += 1
    return updated_value

def double_word_check (word1,word2,group_to_compare,updated_value):

    for ii in range(len(group_to_compare)):
        pronoun_two_words = word1+" "+word2
        if pronoun_two_words == group_to_compare[ii]:
            updated_value += 1
    
    return updated_value

def ds_mma (sentences):

    word_list = []
    no_of_words = []
    pronoun_index = 1
    [pronouns_d,pronouns_s, green_kw, purple_kw,pronouns_col,pronouns_ind] = ds_pronouns()
    pronoun_counter = np.zeros(len(pronouns_s)+len(pronouns_d))

    green_keyword_counter = 0
    green_pronoun_counter = 0
    purple_keyword_counter = 0
    purple_pronoun_counter = 0

    long_words = 0
    short_words = 0
    short_sentences = 0
    long_sentences = 0

    for i in range(len(sentences)):
        word_count = 0
        previous_word = ""
        for word in sentences[i].split():

            word_count += 1
            # filter for additional punctuation
            word_no_punc = ''.join([c for c in word if c not in string.punctuation])

            word_l = len(word_no_punc)
            word_list.append(word_l)

            if len(word_no_punc) > 6:
            	long_words += 1

            if len(word_no_punc) < 7:
            	short_words += 1

            green_pronoun_counter = single_word_check(word_no_punc,pronouns_col,green_pronoun_counter)
            purple_pronoun_counter = single_word_check(word_no_punc,pronouns_ind,purple_pronoun_counter)
            green_keyword_counter = single_word_check(word_no_punc,green_kw,green_keyword_counter)
            purple_keyword_counter = single_word_check(word_no_punc,purple_kw,purple_keyword_counter)

            green_pronoun_counter = double_word_check(previous_word,word_no_punc,pronouns_col,green_pronoun_counter)
            purple_pronoun_counter = double_word_check(previous_word,word_no_punc,pronouns_ind,purple_pronoun_counter)
            green_keyword_counter = double_word_check(previous_word,word_no_punc,green_kw,green_keyword_counter)
            purple_keyword_counter = double_word_check(previous_word,word_no_punc,purple_kw,purple_keyword_counter)

            previous_word = word_no_punc

        max_val2 = 0 
        max_val = 0 
        max_pronoun_id2 = 0  
        max_pronoun_id = 0

        if word_count > 10:
        	long_sentences += 1

        if word_count < 10:
        	short_sentences += 1
             
        no_of_words.append(word_count)


    # min_word = min(word_list)
    # max_word = max(word_list)
    # avg_word = np.mean(word_list)

    # min_no_word = min(no_of_words)
    # max_no_word = max(no_of_words)
    # avg_no_word = np.mean(no_of_words)

    return green_keyword_counter, long_words, long_sentences, green_pronoun_counter, purple_keyword_counter, short_words, short_sentences,purple_pronoun_counter
    # return min_word,max_word,avg_word,min_no_word,max_no_word,avg_no_word,pronoun_counter,max_pronoun_id,max_pronoun_id2,max_val,max_val2


# [min_w,max_w,average_w,min_no_words,max_no_words,avg_no_words,pronoun_counter,max_pronoun_id,max_pronoun_id2,max_val,max_val2] = ds_mma(sentences)

# print max_w,min_w,average_w
# print min_no_words,max_no_words,avg_no_words

# [pronouns_d,pronouns_s] = ds_pronouns()


# print pronoun_counter
# print max_pronoun_id,max_pronoun_id2
# print pronouns_s[max_pronoun_id],pronouns_s[max_pronoun_id2]

# print (max_val/len(sentences)),(max_val2/len(sentences))