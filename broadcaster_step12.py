import zmq
import random
import sys
import time
import re
import string
import numpy as np

port = "5556"
if len(sys.argv) > 1:
    port =  sys.argv[1]
    int(port)

context = zmq.Context()
socket = context.socket(zmq.PUB)
socket.bind("tcp://*:%s" % port)

fileName="Text.txt"
topic = 42
sentence_count = 1
# incrementally building a message
message_data=""

sentences = []
no_of_sentence = 0
word_count = 0
sentence_end_finder=re.compile(r"[\.\?!]")

with open(fileName) as myFile:
    for line in myFile:
        for word in line.split():
            message_data=message_data+" "+word

            word_count +=1

            # print message data when a sentence ends
            if sentence_end_finder.findall(word):

                sentences.append(message_data)
                print no_of_sentence
                no_of_sentence += 1


                print "%d %s" % (topic, message_data)
                socket.send("%d %s" % (topic, message_data))
                message_data=""
                time.sleep(0.05)
        
def ds_pronouns():
    pronoun_list_single =["all","another","any","anybody","anyone","anything","both","each","either","everybody","everyone"
                   "everything","few","he","her","hers","herself","him","himself","his","I","it","its","itself",
    "many"      ,
    "me"        ,
    "mine"      ,
    "more"      ,
    "most"      ,
    "much"      ,
    "myself"    ,
    "neither"   ,
    "no one"    ,
    "nobody"    ,
    "none"      ,
    "nothing"   ,
    "one"       ,
    "other"     ,
    "others"    ,
    "ours"      ,
    "ourselves" ,
    "several"   ,
    "she"       ,
    "some"      ,
    "somebody"  ,
    "someone"   ,
    "something" ,
    "that"      ,
    "their"     ,
    "theirs"    ,
    "them"      ,
    "themselves",
    "these"     ,
    "they"      ,
    "this"      ,
    "those"     ,
    "us"        ,
    "we"        ,
    "what"      ,
    "whatever"  ,
    "which"     ,
    "whichever" ,
    "who"       ,
    "whoever"   ,
    "whom"      ,
    "whomever"  ,
    "whose"     ,
    "you"       ,
    "your"      ,
    "yours"     ,
    "yourself"  ,
    "yourselves"]

    pronoun_list_double  = ["one another","each other"] 

    return pronoun_list_double,pronoun_list_single



def ds_mma (sentences):
    word_list = []
    no_of_words = []
    pronoun_index = 1
    [pronouns_d,pronouns_s] = ds_pronouns()
    pronoun_counter = np.zeros(len(pronouns_s)+len(pronouns_d))

    for i in range(len(sentences)):

        word_count = 0
        previous_word = ""
        for word in sentences[i].split():

            word_count += 1
            # filter for additional punctuation
            word_no_punc = ''.join([c for c in word if c not in string.punctuation])
            print word_no_punc
            print len(word_no_punc)

            word_l = len(word_no_punc)
            word_list.append(word_l)

            for i in range(len(pronouns_s)):
                if word_no_punc == pronouns_s[i]:
                    pronoun_counter[i] += 1 

            for ii in range(len(pronouns_d)):
                pronoun_two_words = previous_word+" "+word_no_punc
                print pronoun_two_words
                if pronoun_two_words == pronouns_d[ii]:
                    pronoun_counter[len(pronouns_s)+1] += 1 

            previous_word = word

        max_val2 = 0 
        max_val = 0 
        max_pronoun_id2 = 0  
        max_pronoun_id = 0 
             
        for ii in xrange(1, len(pronoun_counter)):
            if pronoun_counter[ii] > max_val2:
                max_val2 = pronoun_counter[ii]
                max_pronoun_id2 = ii
            if pronoun_counter[ii] > max_val:
                tmp = max_val2
                max_val2 = max_val
                max_val = tmp

                tmp_id = max_pronoun_id2
                max_pronoun_id2 = max_pronoun_id
                max_pronoun_id = tmp_id

        no_of_words.append(word_count)


    min_word = min(word_list)
    max_word = max(word_list)
    avg_word = np.mean(word_list)

    min_no_word = min(no_of_words)
    max_no_word = max(no_of_words)
    avg_no_word = np.mean(no_of_words)

    return min_word,max_word,avg_word,min_no_word,max_no_word,avg_no_word,pronoun_counter,max_pronoun_id,max_pronoun_id2

[min_w,max_w,average_w,min_no_words,max_no_words,avg_no_words,pronoun_counter,max_pronoun_id,max_pronoun_id2] = ds_mma(sentences)

print max_w,min_w,average_w
print min_no_words,max_no_words,avg_no_words

[pronouns_d,pronouns_s] = ds_pronouns()

print pronouns_s[3]

print pronoun_counter
print max_pronoun_id2,max_pronoun_id
print pronouns_s[max_pronoun_id],pronouns_s[max_pronoun_id2]
print len([1])


#             numb_of_words = len(words)
#     print numb_of_words
#     print words
#     for i in range(len(words)):

#         word = words[i]
#         print word
#         if len(word)

# def ds_avg_min_max(sentences):

#     for i in range(len(sentences))
#         sentences[i]