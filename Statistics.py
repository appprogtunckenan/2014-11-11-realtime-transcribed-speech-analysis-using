import zmq
import random
import sys
import time
import re
import string
import numpy as np

port = "5556"
if len(sys.argv) > 1:
    port =  sys.argv[1]
    int(port)

context = zmq.Context()
socket = context.socket(zmq.PUB)
socket.bind("tcp://*:%s" % port)

sentences = []
i_temp = 0
with open('1.txt') as file:
 	fl = file.read()

 	for i in range(0, len(fl)):
 		if fl[i] == '?' or fl[i] == '!':
 			# print fl[i_temp:i+1]
 			sentences.append(fl[i_temp:i+1])
 			socket.send("%s" % fl[i_temp:i+1])
 			if [i+2] == ' ':
 				i_temp = i+3
 			else:
 				i_temp = i+2
 		if fl[i] == '.':
 			if fl[i-2:i+1] == 'Mr.' or fl[i-3:i+1] == 'Mrs.':
 				continue
 			elif i == len(fl):
 				# print fl[i_temp:i+1]
 				sentences.append(fl[i_temp:i+1])
 				socket.send("%s" % fl[i_temp:i+1])
 				# close file
 			else:
 				# print fl[i_temp:i+1]
 				sentences.append(fl[i_temp:i+1])
 				socket.send("%s" % fl[i_temp:i+1])
 				i_temp = i+2
# print len(sentences)


def ds_pronouns():
    pronoun_list_single =["all","another","any","anybody","anyone","anything","both","each","either","everybody","everyone"
                   "everything","few","he","her","hers","herself","him","himself","his","I","it","its","itself","yous","yall",
    "many"      ,
    "me"        ,
    "mine"      ,
    "more"      ,
    "most"      ,
    "much"      ,
    "myself"    ,
    "neither"   ,
    "no one"    ,
    "nobody"    ,
    "none"      ,
    "nothing"   ,
    "one"       ,
    "other"     ,
    "others"    ,
    "ours"      ,
    "ourselves" ,
    "several"   ,
    "she"       ,
    "some"      ,
    "somebody"  ,
    "someone"   ,
    "something" ,
    "that"      ,
    "their"     ,
    "theirs"    ,
    "them"      ,
    "themselves",
    "these"     ,
    "they"      ,
    "this"      ,
    "those"     ,
    "us"        ,
    "we"        ,
    "what"      ,
    "whatever"  ,
    "which"     ,
    "whichever" ,
    "who"       ,
    "whoever"   ,
    "whom"      ,
    "whomever"  ,
    "whose"     ,
    "you"       ,
    "your"      ,
    "yours"     ,
    "yourself"  ,
    "yourselves"]

    pronoun_list_double  = ["one another","each other"]

    pronoun_list = pronoun_list_single
    for ii in range(len(pronoun_list_double)):
        pronoun_list.append(pronoun_list_double[ii]) 

    return pronoun_list_double,pronoun_list_single,pronoun_list

def ds_mma (sentences):
    word_list = []
    no_of_words = []
    pronoun_index = 1
    [pronouns_d,pronouns_s,pronouns] = ds_pronouns()
    pronoun_counter = np.zeros(len(pronouns_s)+len(pronouns_d))

    for i in range(len(sentences)):

        word_count = 0
        previous_word = ""
        for word in sentences[i].split():

            word_count += 1
            # filter for additional punctuation
            word_no_punc = ''.join([c for c in word if c not in string.punctuation])


            if word_no_punc == "":
                word_count -= 1
            else:
                word_l = len(word_no_punc)
                word_list.append(word_l)

                for i in range(len(pronouns_s)):
                    if word_no_punc == pronouns_s[i] or word_no_punc == pronouns_s[i].capitalize():
                        pronoun_counter[i] += 1

                for ii in range(len(pronouns_d)):
                    pronoun_two_words = previous_word+" "+word_no_punc

                    if pronoun_two_words == pronouns_d[ii] or pronoun_two_words == pronouns_d[ii].capitalize() :
                        pronoun_counter[len(pronouns_s)+1] += 1 

                previous_word = word_no_punc

        max_val2 = 0 
        max_val = 0 
        max_pronoun_id2 = 0  
        max_pronoun_id = 0 
             
        for ii in xrange(1, len(pronoun_counter)):
            if pronoun_counter[ii] > max_val2:
                max_val2 = pronoun_counter[ii]
                max_pronoun_id2 = ii
            if pronoun_counter[ii] > max_val:
                tmp = max_val2
                max_val2 = max_val
                max_val = tmp

                tmp_id = max_pronoun_id2
                max_pronoun_id2 = max_pronoun_id
                max_pronoun_id = tmp_id

        no_of_words.append(word_count)


    min_word = min(word_list)
    max_word = max(word_list)
    avg_word = np.mean(word_list)

    min_no_word = min(no_of_words)
    max_no_word = max(no_of_words)
    avg_no_word = np.mean(no_of_words)

    return min_word,max_word,avg_word,min_no_word,max_no_word,avg_no_word,pronoun_counter,max_pronoun_id,max_pronoun_id2,max_val,max_val2

[min_w,max_w,average_w,min_no_words,max_no_words,avg_no_words,pronoun_counter,max_pronoun_id,max_pronoun_id2,max_val,max_val2] = ds_mma(sentences)
[pronouns_s,pronouns_d,pronouns] = ds_pronouns()

print "Maximum word length: %d" % max_w
print "Minimum word length: %d" % min_w
print "Average word length: %f" % average_w

print "Maximum # of words in a sentence: %d" % max_no_words
print "Minimum # of words in a sentence: %d" % min_no_words
print "Average # of words in a sentence: %f" % avg_no_words

print "Most common pronoun: %s" % pronouns[max_pronoun_id]
print "Frequency per sentence: %f" % (max_val/len(sentences))
print "Second most common pronoun: %s" %  pronouns[max_pronoun_id2]
print "Frequency per sentence: %f" % (max_val2/len(sentences))

