from myTools import *
import sys
import zmq
import matplotlib.pyplot as plt

green_score = 0
green_score_total = 0
green_score_list = []
green_score_list_total = []

class GreenReactor:
	def __init__(self, port):
		# self.keyword_score = 0
		# self.long_word_score = 0
		# self.long_sentence_score = 0
		# self.collective_pronoun_score = 0

		context = zmq.Context()
		socket = context.socket(zmq.SUB)

		socket.setsockopt(zmq.SUBSCRIBE,'')
		socket.connect ("tcp://127.0.0.1:%s" % port)

		print "Green listening..."

		while True:
			string = socket.recv()
			self.onRead(string)

	def getPurpleScore():
		return self.green_score

	def setGreenScore(self, grn_kw, lw, ls,grn_p,sw,ss):
		global green_score
		global green_score_list
		global green_score_total
		global purple_score_list_total

		green_score = 0
		green_score += grn_kw * 15.0
		green_score += lw * 0.5
		green_score += ls * 1.0
		# green_score -= sw * 0.2
		# green_score -= ss * 0.5
		green_score += grn_p * 2.0
		green_score_total += green_score

		green_score_list.append(green_score)
		green_score_list_total.append(green_score_total)

		bins = []
		for ii in range(len(green_score_list)):
			if len(green_score_list)> len(bins):
					bins.append(ii+1)
		plt.ion()
		plt.bar(bins, green_score_list,color='g')
		plt.plot(bins,green_score_list_total,color='g')
		plt.show()
		plt.title("Green Reactor")
		plt.pause(0.01)

		print green_score

	def onRead(self,string):
		global green_score
		sentences = sentence_finder(string)

		# print sentences
		# for i in range(len(sentences)):
		[green_keyword, long_word, long_sentence, green_pronoun,dummy1, short_word, short_sentences,dummy4] = ds_mma(sentences)

		self.setGreenScore(green_keyword, long_word, long_sentence,green_pronoun, short_word, short_sentences)




