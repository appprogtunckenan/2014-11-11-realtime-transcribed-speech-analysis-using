from myTools import *
import sys
import zmq
import matplotlib.pyplot as plt

purple_score = 0
purple_score_total = 0
purple_score_list = []
purple_score_list_total = []

class PurpleReactor:
	def __init__(self, port):
		# self.keyword_score = 0
		# self.short_word_score = 0
		# self.short_sentence_score = 0
		# self.individual_pronoun_score = 0

		context = zmq.Context()
		socket = context.socket(zmq.SUB)

		socket.setsockopt(zmq.SUBSCRIBE,'')
		socket.connect ("tcp://127.0.0.1:%s" % port)

		print "Purple listening..."

		while True:
			string = socket.recv()
			self.onRead(string)
		
	def getPurpleScore():
		return self.purple_score

	def setPurpleScore(self, purp_kw, sw, ss,purp_p,lw,ls):
		global purple_score
		global purple_score_list
		global purple_score_total
		global purple_score_list_total
		purple_score = 0
		purple_score += purp_kw * 15.0
		purple_score += sw * 0.5
		purple_score += ss * 1.0
		purple_score -= lw * 2.0
		purple_score -= ls * 3.0
		purple_score += purp_p * 2.0

		purple_score_total += purple_score

		purple_score_list.append(purple_score)
		purple_score_list_total.append(purple_score_total)
		bins_purple = []
		for ii in range(len(purple_score_list)):
			if len(purple_score_list)> len(bins_purple):
					bins_purple.append(ii+1)
			
		plt.ion()
		plt.bar(bins_purple, purple_score_list,color='m')
		plt.plot(bins_purple,purple_score_list_total,color='m')
		plt.show()
		plt.title("Purple Reactor")
		plt.pause(0.01)


		print purple_score

	def onRead(self,string):
		global purple_score
		sentences = sentence_finder(string)


		# for i in range(len(sentences)):
		[dummy1, long_word, long_sentence, dummy4, purple_keyword, short_word, short_sentence,purple_pronoun] = ds_mma(sentences)

		self.setPurpleScore(purple_keyword, short_word, short_sentence,purple_pronoun, long_word, long_sentence)

