import sys
import zmq
import matplotlib.pyplot as plt
from PurpleReactor import*
from GreenReactor import*

global green_score_list
global purple_score_list

bins = []
for ii in range(len(green_score_list)):
	bins.append(ii+1)
plt.ion()
plt.bar(bins+0.2, green_score_list,width=0.4,color='g',align='center')
plt.bar(bins-0.2, purple_score_list,width=0.4,color='r',align='center')
plt.show()
plt.title("Green Reactor")
plt.pause(0.01)